import { useState, useRef } from "react";
import "./SearchBox.css";
import axios from "axios";
import { useDispatch, useSelector } from "react-redux";
import { getUserState } from "../../store/slices/userSlice";
import { getChatState, getChats } from "../../store/slices/chatSlice";
import { IoSend } from "react-icons/io5";
import { addCurrentChat } from "../../store/slices/currentChatSlice";
import { getSocketState } from "../../store/slices/socketSlice";
import { ToastContainer, toast } from "react-toastify";
import { toastConfig } from "../../Utils/constants";
import { UserDetails } from "../../typings/types";
import { AppDispatch } from "../../store/store";

const SearchBox = () => {
  const inputRef = useRef<HTMLInputElement>(null);
  const dispatch = useDispatch<AppDispatch>();

  const { userId, userName, avatarImage } = useSelector(getUserState);

  const id = userId;
  const name = userName;
  const image = avatarImage;

  const { chats } = useSelector(getChatState);
  const [showModal, setShowModal] = useState(false);
  const [allUsers, setAllUsers] = useState([]);
  // input (first message)

  // redux socket
  const { socket } = useSelector(getSocketState);
  const [selectedUser, setSelectedUser] = useState<
    UserDetails & { _id: string }
  >();

  // Function to handle search
  const handleSearch = async () => {
    const users = await axios.get(`/get-all-users/${userId}`);

    const filteredUsers = users.data.users.filter(
      (user: UserDetails & { _id: string }) => {
        return !chats.some((chat) => chat.userId === user._id);
      }
    );
    setAllUsers(filteredUsers);
    setShowModal(true);
  };

  const submitHandler = async () => {
    const msg = inputRef?.current?.value;
    if (msg?.trim() === "") {
      return;
    }
    const details = {
      senderId: userId,
      receiverId: selectedUser?._id,
      message: msg,
    };
    try {
      const res = await axios.post("/first-message", details);
      if (res.status === 201) {
        const chatId = res.data.chatId;
        const chatUserId = selectedUser?._id;
        const avatarImage = selectedUser?.avatarImage;
        const username = selectedUser?.username;

        setShowModal(false);
        dispatch(getChats(userId));
        dispatch(
          addCurrentChat({ chatId, userId: chatUserId, avatarImage, username })
        );

        socket.emit("SEND_FIRST_MESSAGE", {
          userId: id,
          username: name,
          avatarImage: image,
          chatId: chatId,
          receiverId: selectedUser?._id,
        });
      }
    } catch (error: any) {
      toast.error(error?.data?.message, toastConfig);
    }
  };

  return (
    <div style={{ position: "relative" }}>
      <input
        type="text"
        className="search-box"
        onFocus={handleSearch}
        placeholder="Search users for new chat..."
      />

      {showModal && (
        <div className="modal-overlay">
          <div className="modal-custom">
            <button
              style={{ fontSize: "10px" }}
              className="close-button"
              onClick={() => setShowModal(false)}
            >
              X
            </button>
            <ul>
              {allUsers.map((user: UserDetails & { _id: string }) => (
                <div
                  key={user._id}
                  onClick={() => {
                    setSelectedUser(user);
                  }}
                  style={{ display: "flex", flexDirection: "column" }}
                >
                  <div
                    style={{
                      display: "flex",
                      flexDirection: "row",
                      margin: "4%",
                      cursor: "pointer",
                    }}
                  >
                    <img
                      className="chat-item-image"
                      src={user?.avatarImage}
                      alt="image"
                    />
                    <li style={{ listStyleType: "none", fontSize: "20px" }}>
                      {user.username}
                    </li>
                  </div>
                  {selectedUser?._id === user._id && (
                    <div
                      style={{
                        marginTop: "5%",
                        display: "flex",
                        flexDirection: "row",
                        cursor: "pointer",
                        justifyContent: "space-around",
                      }}
                    >
                      <input
                        style={{ width: "40vh" }}
                        placeholder="Enter your first message"
                        type="text"
                        ref={inputRef}
                      />
                      <button onClick={submitHandler}>
                        <IoSend />
                      </button>
                    </div>
                  )}
                  <hr />
                </div>
              ))}
            </ul>
          </div>
        </div>
      )}
      <ToastContainer />
    </div>
  );
};

export default SearchBox;
