import axios from "axios";
import React, { useEffect, useRef, useState, MouseEvent } from "react";
import { useSelector } from "react-redux";
import { getCurrentChatState } from "../../store/slices/currentChatSlice";
import getTime from "../../Utils/getTime";
import robot from "../../assets/robot.gif";

import { IoSend } from "react-icons/io5";
import { BsEmojiSmileFill } from "react-icons/bs";

import Picker, { EmojiClickData } from "emoji-picker-react";
import { useOnClickOutside } from "../../Utils/useOutsideClick";
// loader
import { Audio } from "react-loader-spinner";
import { getUserState } from "../../store/slices/userSlice";
import { ToastContainer, toast } from "react-toastify";
import { toastConfig } from "../../Utils/constants";
import { ChatProps, ReceiveMsgEmit } from "../../typings/types";

const ChatWindow: React.FC<ChatProps> = ({ onlineUsers, socket, lastSeenUsers}) => {
  const ref = useRef(null);
  const [msg, setMsg] = useState("");
  useOnClickOutside(ref, () => setShowEmojiPicker(false));
  const scrollRef = useRef<HTMLDivElement>(null);

  const { userId } = useSelector(getUserState);

  const {
    currentChatId,
    chatUserId,
    chatUserAvatarImage,
    chatUserName,
  } = useSelector(getCurrentChatState);

  const [messages, setMessages] = useState<ReceiveMsgEmit[]>([]);
  const [isLoading, setIsLoading] = useState(false);
  const [showEmojiPicker, setShowEmojiPicker] = useState(false);

  const handleEmojiPickerHideShow = () => {
    setShowEmojiPicker((prev) => !prev);
  };

  const handleEmojiClick = (emoji: EmojiClickData) => {
    setShowEmojiPicker(false);
    let message = msg;
    message += emoji.emoji;
    setMsg(message);
  };

  const getMessages = async () => {
    try {
      setIsLoading(true);
      const result = await axios.get(`/get-messages/${currentChatId}`);
      setMessages(result.data.messages);
    } catch (error) {
    } finally {
      setIsLoading(false);
    }
  };

  useEffect(() => {
    if (currentChatId !== "") {
      getMessages();
    }
  }, [currentChatId]);

  useEffect(() => {
    scrollRef.current?.scrollIntoView({ behavior: "smooth" });
  }, [messages]);

  useEffect(() => {
    socket.on("RECEIVE_MESSAGE", (data: ReceiveMsgEmit) => {
      if (data.chatId === currentChatId) setMessages((prev) => [...prev, data]);
    });
    return () => {
      socket.off("RECEIVE_MESSAGE");
    };
  }, [currentChatId, socket]);

  const onlineCheck = (receiverId: string) => {
    return onlineUsers.includes(receiverId);
  };

  const sendMessageHandler = async (
    event: MouseEvent<HTMLButtonElement> | React.FormEvent<HTMLFormElement>
  ) => {
    event.preventDefault();
    if (msg.trim() === "") {
      return;
    }
    const msgDetails = {
      senderId: userId,
      chatId: currentChatId,
      message: msg,
    };
    try {
      const res = await axios.post("/post-message", msgDetails);
      if (res.status === 201) {
        socket.emit("SEND_MESSAGE", {
          chatId: currentChatId,
          senderId: userId,
          content: msg,
          timeStamp: Date.now(),
          readReceipt: [],
          receiverId: chatUserId,
        });
        const msgs: ReceiveMsgEmit[] = [
          ...messages,
          {
            chatId: currentChatId,
            senderId: userId,
            content: msg,
            timeStamp: Date.now(),
            readReceipt: [],
          },
        ];
        setMessages(msgs);
        setMsg("");
      }
    } catch (error: any) {
      toast.error(error.data.message, toastConfig);
    }
  };

  if (isLoading) {
    return (
      <div>
        <Audio height="100" width="100" color="blue" ariaLabel="loading" />
      </div>
    );
  }

  const isOnline = onlineCheck(chatUserId);
  const chatUserLastSeen = lastSeenUsers.get(chatUserId)

  const userStatus = isOnline
    ? "Active"
    : new Date().toDateString() === new Date(chatUserLastSeen).toDateString()
    ? `Last seen : ${getTime(new Date(chatUserLastSeen))}`
    : `Last seen : ${getTime(new Date(chatUserLastSeen))} ${new Date(
        chatUserLastSeen
      ).toLocaleDateString()}`;

  return (
    <div className="chat-window">
      {currentChatId && !isLoading ? (
        <>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <img
              className={
                isOnline ? `chat-item-image profile-online` : "chat-item-image"
              }
              src={chatUserAvatarImage}
              alt="image"
            />
            <div style={{ display: "flex", flexDirection: "column" }}>
              <h2 style={{ fontSize: "25px", marginTop: "-7%" }}>
                {chatUserName}
              </h2>
              {isOnline && (
                <span style={{ fontSize: "13px", marginTop: "-12%" }}>
                  Active
                </span>
              )}
              {!isOnline && (
                <span style={{ fontSize: "13px", marginTop: "-5%" }}>
                  {userStatus}
                </span>
              )}
            </div>
          </div>
          <div className="messages">
            {messages.map((message, idx) => (
              <div
                ref={scrollRef}
                key={idx}
                className={`message ${
                  message?.senderId === chatUserId ? "receiver" : "user"
                }`}
              >
                <p>{message?.content}</p>
                <span className="time">
                  {getTime(new Date(message.timeStamp))}
                </span>
              </div>
            ))}
          </div>
          <div className="send-message-container">
            <div className="emoji">
              <BsEmojiSmileFill
                onClick={handleEmojiPickerHideShow}
                onClickOutside={() => setShowEmojiPicker(false)}
              />
              {showEmojiPicker && (
                <div className="emoji-expanded" ref={ref}>
                  <Picker onEmojiClick={handleEmojiClick} />
                </div>
              )}
            </div>
            <form style={{ flex: "1" }} onSubmit={sendMessageHandler}>
              <input
                onChange={(e) => {
                  setMsg(e.target.value);
                }}
                type="text"
                placeholder="Send a message..."
                className="send-message-input"
                value={msg}
              />
            </form>
            <button
              onClick={sendMessageHandler}
              className="send-message-button"
            >
              <IoSend />
            </button>
          </div>
        </>
      ) : (
        !isLoading && (
          <div className="no-chat-selected">
            <img
              style={{ height: "60vh", width: "60vh" }}
              src={robot}
              alt="No Chat Selected"
            />
            <p>Select a chat to start messaging</p>
          </div>
        )
      )}
      <ToastContainer />
    </div>
  );
};

export default ChatWindow;
