import { useEffect, useState } from "react";
import "./chat.css";
import { useDispatch, useSelector } from "react-redux";
import {
  appendChat,
  getChatState,
  getChats,
} from "../../store/slices/chatSlice";
import { getUserState } from "../../store/slices/userSlice";
import { addCurrentChat } from "../../store/slices/currentChatSlice";
import ChatWindow from "../components/ChatWindow";
import { LOADING, toastConfig } from "../../Utils/constants";
import { Audio } from "react-loader-spinner";

// socket
import io from "socket.io-client";

import { ToastContainer, toast } from "react-toastify";
import { addSocket } from "../../store/slices/socketSlice";
import { AppDispatch } from "../../store/store";

const Chat = () => {
  const { userId } = useSelector(getUserState);
  const dispatch = useDispatch<AppDispatch>();
  const { chats, status, error } = useSelector(getChatState);

  const [onlineUsers, setOnlineUsers] = useState<Array<string>>([]);
  const [socket, setSocket] = useState<Object>();
  // let lastSeenUsers = new Map();
  const [lastSeenUsers, setLastSeenUsers] = useState(new Map());
  // const currentChatIdRef= useRef();
  useEffect(() => {
    dispatch(getChats(userId));
  }, []);

  useEffect(() => {
    if (userId !== "") {
      const socket = io("http://localhost:8080/user-namespace", {
        auth: { userId },
      });
      if (socket === null) return;

      socket.on("connect", () => {
        console.log("Connected to socket IO server");
      });
      socket.on("GET_ONLINE_USERS", (res) => {
        setOnlineUsers(res);
      });
      socket.on("LAST_SEEN", (data: any) => {
        setLastSeenUsers(new Map(data));
      });
      socket.on("RECEIVE_CHAT", (data) => {
        dispatch(appendChat(data));
      });
      // Push data to users
      setSocket(socket);
      dispatch(addSocket(socket));
      return () => {
        socket.off("SEND_FIRST_MESSAGE");
      };
    }
  }, [userId]);

  const onlineCheck = (receiverId: string) => {
    return onlineUsers.includes(receiverId);
    // return onlineUsers.find((id)=>id===receiverId);
  };

  if (status === LOADING) {
    return (
      <center>
        <Audio height="100" width="100" color="blue" ariaLabel="loading" />
      </center>
    );
  }

  if (error) {
    return toast.error(error, toastConfig);
  }

  return (
    <div className="app">
      <div className="chat-container">
        <div className="chat-list">
          {chats.length === 0 && (
            <center>
              <img
                style={{ width: "70%", marginTop: "30%" }}
                src="https://cdni.iconscout.com/illustration/premium/thumb/no-messages-8044859-6430768.png"
              />
              <p style={{ fontSize: "20px" }}>No chats available</p>
              <p style={{ fontSize: "20px" }}>Start a new conversation</p>
            </center>
          )}
          {chats.length !== 0 &&
            chats.map((chat) => (
              <div
                key={chat.chatId}
                onClick={() => {
                  dispatch(
                    addCurrentChat({
                      ...chat,
                    })
                  );
                }}
                className="chat-item"
              >
                <img
                  className={
                    onlineCheck(chat.userId)
                      ? `chat-item-image profile-online`
                      : "chat-item-image"
                  }
                  src={chat.avatarImage}
                  alt="image"
                />
                <h3>{chat.username}</h3>
              </div>
            ))}
        </div>
        {socket && (
          <ChatWindow
            onlineUsers={onlineUsers}
            socket={socket}
            lastSeenUsers={lastSeenUsers}
          />
        )}
        <ToastContainer />
      </div>
    </div>
  );
};

export default Chat;
