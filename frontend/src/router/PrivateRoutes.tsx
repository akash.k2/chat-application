import { useEffect } from "react";

import { Navigate, Outlet, useNavigate } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import { ErrorBoundary } from "react-error-boundary";
import { fetchUserDetails, getUserState } from "../store/slices/userSlice";
import {
  FAILED,
  FORBIDDEN,
  LOADING,
  SUCCEEDED,
  IDLE,
} from "../Utils/constants";

import { FiLogOut } from "react-icons/fi";
import { BsTwitter } from "react-icons/bs";
import SearchBox from "../Chats/components/SearchBox";
import { logOutUser } from "../Utils/logoutHandler";
import { getSocketState } from "../store/slices/socketSlice";
import { AppDispatch } from "../store/store";
import Unauthorized from "../Auth/pages/Unauthorized";

const PrivateRoutes = () => {
  const dispatch = useDispatch<AppDispatch>();
  const navigate = useNavigate();

  let authToken = sessionStorage.getItem("accessToken");

  if (!authToken) {
    <Navigate to="/login" />;
  }

  const { status, error, userName, avatarImage } = useSelector(getUserState);

  const { socket } = useSelector(getSocketState);

  useEffect(() => {
    if (status === IDLE) {
      dispatch(fetchUserDetails());
    }
  }, []);

  if (status === LOADING) {
    return null;
  }

  // optional one since it was handled in Error boundary
  if (status === FAILED || error === FORBIDDEN.message) {
    return <Navigate to="/login" />;
  }

  if (status === SUCCEEDED) {
    return (
      <ErrorBoundary FallbackComponent={Unauthorized}>
        <div className="navbar">
          <h2>
            <BsTwitter /> Birdy
          </h2>
          <div className="chat-item-nav">
            <div>
              <SearchBox />
            </div>
            <div className="img-div">
              <img className="chat-item-image-nav" src={avatarImage} />
              <h3>{userName}</h3>
            </div>
            <button
              onClick={() => {
                logOutUser(socket, navigate);
              }}
            >
              <h3>
                <FiLogOut />
              </h3>
            </button>
          </div>
        </div>
        <Outlet />
      </ErrorBoundary>
    );
  }
};
export default PrivateRoutes;
