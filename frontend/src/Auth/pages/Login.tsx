import {
  MDBBtn,
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBInput,
  MDBIcon,
  MDBCheckbox,
} from "mdb-react-ui-kit";
import { Form } from "reactstrap";

import { Link, useNavigate } from "react-router-dom";
import { ChangeEvent, useState, FormEvent } from "react";

// Toast
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { SUCCEEDED, toastConfig } from "../../Utils/constants";

import { useDispatch, useSelector } from "react-redux";
import { getUserState, userLogin } from "../../store/slices/userSlice";
import { AppDispatch } from "../../store/store";

const Login = () => {
  // dispatch
  const dispatch = useDispatch<AppDispatch>();
  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [showPassword, setShowPassword] = useState(false);

  const nameHandler = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.value.trim().length === 0) {
      toast.error("Enter name first", toastConfig);
      return;
    }
    setName(event.target.value);
  };

  const passwordHandler = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.value.trim().length === 0) {
      toast.error("Enter password", toastConfig);
      return;
    }
    setUserPassword(event.target.value);
  };

  const checkBoxHandler = () => {
    setShowPassword((showPassword) => !showPassword);
  };

  const loginHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const userData = { username: name, password: userPassword };
    dispatch(userLogin(userData));
  };

  const { status, error } = useSelector(getUserState);

  if (status === SUCCEEDED) {
    navigate("/chat");
    return;
  }

  if (error) {
    toast.error(error, toastConfig);
  }

  return (
    <MDBContainer fluid>
      <MDBCard className="text-black m-5" style={{ borderRadius: "25px" }}>
        <Form onSubmit={loginHandler}>
          <MDBCardBody>
            <MDBRow>
              <MDBCol
                md="10"
                lg="6"
                className="order-2 order-lg-1 d-flex flex-column align-items-center"
              >
                <p className="text-center h2 mb-4 mx-1 mx-md-3 mt-3">Sign in</p>

                <div className="d-flex flex-row align-items-center mb-4">
                  <MDBIcon fas icon="user me-3" size="lg" />
                  <MDBInput
                    label="Username"
                    id="form2"
                    type="text"
                    size="lg"
                    onBlur={nameHandler}
                    onChange={nameHandler}
                  />
                </div>

                <div className="d-flex flex-row align-items-center mb-4">
                  <MDBIcon fas icon="lock me-3" size="lg" />
                  <MDBInput
                    label="Password"
                    id="form3"
                    type={showPassword ? "text" : "password"}
                    size="lg"
                    onBlur={passwordHandler}
                    onChange={passwordHandler}
                  />
                </div>

                <div className="mb-4">
                  <MDBCheckbox
                    name="flexCheck"
                    value=""
                    id="flexCheckDefault"
                    label="Show Password"
                    onChange={checkBoxHandler}
                  />
                </div>

                <MDBBtn className="mb-2" size="lg">
                  Login
                </MDBBtn>

                <div>
                  <p className="text-center h5 mb-4 mx-1 mx-md-3 mt-3">
                    New user?{" "}
                    <Link style={{ textDecoration: "none" }} to="/register">
                      Register
                    </Link>
                  </p>
                </div>
              </MDBCol>

              <MDBCol
                md="10"
                lg="6"
                className="order-1 order-lg-2 d-flex align-items-center"
              >
                <MDBCardImage
                  style={{ height: "72vh", margin: "auto" }}
                  src="https://img.freepik.com/premium-vector/illustration-vector-graphic-cartoon-character-login_516790-1261.jpg"
                  fluid
                />
              </MDBCol>
            </MDBRow>
          </MDBCardBody>
        </Form>
      </MDBCard>
      <ToastContainer />
    </MDBContainer>
  );
};

export default Login;
