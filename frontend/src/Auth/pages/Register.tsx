import { useEffect, useState, ChangeEvent, FormEvent } from "react";
import {
  MDBBtn,
  MDBContainer,
  MDBRow,
  MDBCol,
  MDBCard,
  MDBCardBody,
  MDBCardImage,
  MDBInput,
  MDBIcon,
  MDBCheckbox,
} from "mdb-react-ui-kit";
import { Link, useNavigate } from "react-router-dom";
import { Form } from "reactstrap";

// Toast
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.css";
import { SUCCEEDED, toastConfig } from "../../Utils/constants";
import { useDispatch, useSelector } from "react-redux";
import { getUserState, userRegister } from "../../store/slices/userSlice";
import { AppDispatch } from "../../store/store";

const Register = () => {
  const dispatch = useDispatch<AppDispatch>();
  const navigate = useNavigate();

  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [imageUrl, setImageUrl] = useState("");
  const [userPassword, setUserPassword] = useState("");
  const [userPasswordAgain, setUserPasswordAgain] = useState("");
  const [showPassword, setShowPassword] = useState(false);
  const [loading, setLoading] = useState(false);

  const api = "https://api.multiavatar.com";
  const [avatars, setAvatars] = useState<string[]>([]);

  const getAvatars = async () => {
    setLoading(true);
    const data: string[] = [];
    try {
      for (let i = 0; i < 4; i++) {
        data.push(`${api}/${Math.round(Math.random() * 1000)}.png`);
      }
      setAvatars(data);
    } catch (error: any) {
      toast.error(error, toastConfig);
    } finally {
      setLoading(false);
    }
    setAvatars(data);
  };

  useEffect(() => {
    getAvatars();
  }, []);

  const nameHandler = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.value.trim().length === 0) {
      toast.error("Enter name first", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    setName(event.target.value);
  };

  const mailHandler = (event: ChangeEvent<HTMLInputElement>) => {
    if (event.target.value.trim().length === 0) {
      toast.error("Enter email first", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    setEmail(event.target.value);
  };

  const passwordHandler = (event: ChangeEvent<HTMLInputElement>) => {
    const pass = event.target.value.trim();
    let regex =
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;

    if (pass.trim().length === 0 && regex.test(pass)) {
      toast.error("Enter strong password", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    setUserPassword(pass);
  };

  const passwordAgainHandler = (event: ChangeEvent<HTMLInputElement>) => {
    const pass = event.target.value.trim();
    let regex =
      /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$/;

    if (pass.trim().length === 0 && regex.test(pass)) {
      toast.error("Enter strong password", {
        position: toast.POSITION.TOP_RIGHT,
      });
    }
    setUserPasswordAgain(pass);
  };

  const checkBoxHandler = () => {
    setShowPassword((showPassword) => !showPassword);
  };

  const registerHandler = (event: FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    if (userPassword !== userPasswordAgain) {
      toast.error("Confirm password must be same as Password", toastConfig);
      return;
    }
    if (imageUrl === "") {
      toast.error("Choose any one avatar", toastConfig);
      return;
    }
    const userData = {
      username: name,
      email: email,
      password: userPassword,
      avatarImage: imageUrl,
    };
    dispatch(userRegister(userData));
  };

  const { status, error } = useSelector(getUserState);

  if (status === SUCCEEDED) {
    navigate("/chat");
    return;
  }

  if (error) {
    toast.error(error, toastConfig);
  }

  return (
    !loading && (
      <MDBContainer fluid>
        <MDBCard className="text-black m-5" style={{ borderRadius: "25px" }}>
          <Form onSubmit={registerHandler}>
            <MDBCardBody>
              <MDBRow>
                <MDBCol
                  md="10"
                  lg="6"
                  className="order-2 order-lg-1 d-flex flex-column align-items-center"
                >
                  <p className="text-center h2 mb-4 mx-1 mx-md-3 mt-3">
                    Sign up
                  </p>

                  <div className="d-flex flex-row align-items-center mb-4 ">
                    <MDBIcon fas icon="user me-3" size="lg" />
                    <MDBInput
                      label="Username"
                      id="form1"
                      type="text"
                      className="w-100"
                      size="lg"
                      onBlur={nameHandler}
                      onChange={nameHandler}
                    />
                  </div>

                  <div className="d-flex flex-row align-items-center mb-4">
                    <MDBIcon fas icon="envelope me-3" size="lg" />
                    <MDBInput
                      label="Your Email"
                      id="form2"
                      type="email"
                      size="lg"
                      onBlur={mailHandler}
                      onChange={mailHandler}
                    />
                  </div>

                  <div className="d-flex flex-row align-items-center mb-4">
                    <MDBIcon fas icon="lock me-3" size="lg" />
                    <MDBInput
                      label="Password"
                      id="form3"
                      type={showPassword ? "text" : "password"}
                      size="lg"
                      onBlur={passwordHandler}
                      onChange={passwordHandler}
                    />
                  </div>

                  <div className="d-flex flex-row align-items-center mb-4">
                    <MDBIcon fas icon="key me-3" size="lg" />
                    <MDBInput
                      label="Repeat your password"
                      id="form4"
                      type={showPassword ? "text" : "password"}
                      size="lg"
                      onBlur={passwordAgainHandler}
                      onChange={passwordAgainHandler}
                    />
                  </div>

                  <div className="mb-4">
                    <MDBCheckbox
                      name="flexCheck"
                      value=""
                      id="flexCheckDefault"
                      label="Show Password"
                      onClick={checkBoxHandler}
                    />
                  </div>

                  <p style={{ fontSize: "20px" }}>Select your avatar</p>
                  <div className="d-flex flex-row align-items-center mb-4">
                    {avatars.map((avatar, idx) => {
                      return (
                        <img
                          key={idx}
                          onClick={() => {
                            setImageUrl(avatar);
                          }}
                          style={{ cursor: "pointer" }}
                          className={`chat-item-image ${
                            imageUrl === avatar ? " profile-online" : ""
                          }`}
                          src={avatar}
                        />
                      );
                    })}
                  </div>

                  <MDBBtn className="mb-2" size="lg">
                    Register
                  </MDBBtn>

                  <div>
                    <p className="text-center h5 mb-4 mx-1 mx-md-3 mt-3">
                      Already an user ?{" "}
                      <Link style={{ textDecoration: "none" }} to="/login">
                        Login
                      </Link>
                    </p>
                  </div>
                </MDBCol>

                <MDBCol
                  md="10"
                  lg="6"
                  className="order-1 order-lg-2 d-flex align-items-center"
                >
                  <MDBCardImage
                    src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-registration/draw1.webp"
                    fluid
                  />
                </MDBCol>
              </MDBRow>
            </MDBCardBody>
          </Form>
        </MDBCard>
        <ToastContainer />
      </MDBContainer>
    )
  );
};

export default Register;
