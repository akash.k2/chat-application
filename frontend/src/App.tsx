import { Routes, Route, Navigate } from "react-router-dom";
import "mdb-react-ui-kit/dist/css/mdb.min.css";
import "@fortawesome/fontawesome-free/css/all.min.css";

import Register from "./Auth/pages/Register";
import Login from "./Auth/pages/Login";

import PrivateRoutes from "./router/PrivateRoutes";
import Chat from "./Chats/pages/Chat";

const App = () => {
  return (
    <Routes>
      <Route path="/" exact element={<Navigate to="/login" />} />
      <Route path="/login" exact element={<Login />} />
      <Route path="/register" exact element={<Register />} />
      <Route element={<PrivateRoutes />}>
        <Route path="/chat" exact element={<Chat />} />
      </Route>
    </Routes>
  );
};

export default App;
