export type LoginInput = {
  username: string;
  password: string;
};

export type UserDetails = {
  id: string;
  username: string;
  avatarImage: string;
};

export type LoginOutput = {
  message: string;
  token: string;
  details: UserDetails;
};

export type RegisterInput = {
  username: string;
  email: string;
  password: string;
  avatarImage: string;
};

export type ChatProps = {
  onlineUsers: string[];
  socket: any;
  lastSeenUsers : any;
};

export type ReceiveMsgEmit = {
  chatId: string;
  senderId: string;
  timeStamp: number;
  content: string;
  readReceipt: [];
};

export type UserSlice = {
  userName: string;
  userId: string;
  avatarImage: string;
  status: string;
  error: null | string;
};

export type CurrentChatSlice = {
  currentChatId: string;
  chatUserName: string;
  chatUserId: string;
  chatUserAvatarImage: string;
};

export type Chats = {
  userId: string;
  username: string;
  avatarImage: string;
  chatId: string;
};

export type ChatSlice = {
  chats: Array<Chats>;
  status: string;
  error: string | null;
};

export type RootState = {
  user: UserSlice;
  socket: any;
  chats: ChatSlice;
  currentChat: CurrentChatSlice;
};

export type MyAxiosError = {
  message: string;
  status: number;
};
