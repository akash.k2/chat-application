import axios from "axios";

axios.defaults.baseURL = import.meta.env.VITE_BACKEND_URL;

axios.interceptors.request.use(function (config) {
  const accessToken = sessionStorage.getItem("accessToken");
  if (config.url !== "login" && config.url !== "register") {
    config.headers["Authorization"] = `Bearer ${accessToken}`;
  }
  return config;
});

axios.interceptors.response.use(
  (res) => res,
  async (err) => {
    if (
      (err.response.status === 401 && sessionStorage.getItem("accessToken")) ||
      (err.response.data.error && sessionStorage.getItem("accessToken"))
    ) {
      sessionStorage.clear();
      window.location.reload();
    }
    return Promise.reject(err);
  }
);

export default axios;
