import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import {
  FAILED,
  FORBIDDEN,
  IDLE,
  LOADING,
  SUCCEEDED,
} from "../../Utils/constants";
import axios, { AxiosError } from "axios";
import {
  LoginInput,
  RegisterInput,
  RootState,
  UserDetails,
  UserSlice,
} from "../../typings/types";

const initialState: UserSlice = {
  userName: "",
  userId: "",
  avatarImage: "",
  status: IDLE,
  error: null,
};

export const userRegister = createAsyncThunk<UserDetails, RegisterInput, any>(
  "user/userRegister",
  async (details) => {
    try {
      const res = await axios.post("/register", details);
      if (res.status === 201) {
        sessionStorage.setItem("accessToken", res.data.token);
        return res.data.details;
      }
    } catch (error: any) {
      const err = error as AxiosError;
      err.message = error?.response?.data?.message;
      throw err;
    }
  }
);

export const userLogin = createAsyncThunk<UserDetails, LoginInput, any>(
  "user/userLogin",
  async (details) => {
    try {
      const res = await axios.post("/login", details);
      if (res.status === 200) {
        sessionStorage.setItem("accessToken", res.data.token);
        return res.data.details;
      }
    } catch (error: any) {
      const err = error;
      err.message = error.response?.data?.message;
      throw err;
    }
  }
);

export const fetchUserDetails = createAsyncThunk<UserDetails>(
  "user/fetchUserDetails",
  async () => {
    try {
      const res = await axios.get("/get-details");
      return res.data.details;
    } catch (error: any) {
      const err = error;
      err.message = FORBIDDEN.message;
      throw err;
    }
  }
);

export const userLogout = createAsyncThunk(
  "user/userLogout",
  async (userId) => {
    try {
      const res = await axios.post("/logout", { userId });
      if (res.status === 200) {
        sessionStorage.clear();
      }
    } catch (error: any) {
      const err = error;
      err.message = error.response.data?.message;
      throw err;
    }
  }
);

const userSlice = createSlice({
  name: "user",
  initialState,
  reducers: {
    removeUser: (state) => {
      state.userName = "";
      state.userId = "";
      state.avatarImage = "";
      state.status = IDLE;
      state.error = null;
    },
  },
  extraReducers(builder) {
    builder
      //login
      .addCase(userLogin.pending, (state) => {
        state.userName = "";
        state.userId = "";
        state.avatarImage = "";
        state.status = LOADING;
        state.error = null;
      })
      .addCase(userLogin.fulfilled, (state, action) => {
        state.userName = action.payload.username;
        state.userId = action.payload.id;
        state.avatarImage = action.payload.avatarImage;
        state.status = SUCCEEDED;
        state.error = null;
      })
      .addCase(userLogin.rejected, (state, action) => {
        state.userName = "";
        state.userId = "";
        state.avatarImage = "";
        state.status = FAILED;
        state.error = action?.error?.message as string;
      })

      //register
      .addCase(userRegister.pending, (state) => {
        state.userName = "";
        state.userId = "";
        state.avatarImage = "";
        state.status = LOADING;
        state.error = "";
      })
      .addCase(userRegister.fulfilled, (state, action) => {
        state.userName = action.payload.username;
        state.userId = action.payload.id;
        state.avatarImage = action.payload.avatarImage;
        state.status = SUCCEEDED;
        state.error = "";
      })
      .addCase(userRegister.rejected, (state, action) => {
        state.userName = "";
        state.userId = "";
        state.avatarImage = "";
        state.status = FAILED;
        state.error = action.error.message as string;
      })

      //fetchUserDetails
      .addCase(fetchUserDetails.pending, (state) => {
        state.status = LOADING;
        state.error = "";
        state.userId = "";
        state.userName = "";
        state.avatarImage = "";
      })
      .addCase(fetchUserDetails.fulfilled, (state, action) => {
        state.status = SUCCEEDED;
        state.error = "";
        state.userName = action.payload.username;
        state.userId = action.payload.id;
        state.avatarImage = action.payload.avatarImage;
      })
      .addCase(fetchUserDetails.rejected, (state, action) => {
        state.userName = "";
        state.userId = "";
        state.avatarImage = "";
        state.status = FAILED;
        state.error = action.error.message as string;
      })

      //logout
      .addCase(userLogout.pending, (state) => {
        state.userName = state.userName;
        state.userId = state.userId;
        state.avatarImage = state.avatarImage;
        state.status = IDLE;
        state.error = "";
      })
      .addCase(userLogout.fulfilled, (state) => {
        state.userName = "";
        state.userId = "";
        state.avatarImage = "";
        state.status = IDLE;
        state.error = "";
      })
      .addCase(userLogout.rejected, (state, action) => {
        state.userName = state.userName;
        state.userId = state.userId;
        state.avatarImage = state.avatarImage;
        state.status = FAILED;
        state.error = action.error.message as string;
      });
  },
});

// for use selector
export const getUserState = (state: RootState) => state.user;

export const { removeUser } = userSlice.actions;

export const userReducer = userSlice.reducer;
