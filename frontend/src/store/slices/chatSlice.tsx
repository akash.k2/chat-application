import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import { FAILED, IDLE, LOADING, SUCCEEDED } from "../../Utils/constants";
import axios, { AxiosError } from "axios";
import { ChatSlice, Chats, RootState } from "../../typings/types";

const initialState: ChatSlice = {
  chats: [],
  status: IDLE,
  error: null,
};

export const getChats = createAsyncThunk<Chats[], string, any>(
  "chats/getChats",
  async (userId) => {
    try {
      const res = await axios.get(`get-friends/${userId}`);
      if (res.status === 200) {
        return res.data.chats;
      } else if (res.status === 204) {
        return [];
      }
    } catch (error: any) {
      const err = error as AxiosError;
      err.message = error?.response?.data?.message;
      throw err;
    }
  }
);

const chatSlice = createSlice({
  name: "chats",
  initialState,
  reducers: {
    appendChat: (state, action) => {
      state.chats = [action.payload, ...state.chats];
    },
  },

  extraReducers: (builder) => {
    builder
      .addCase(getChats.pending, (state) => {
        state.chats = [];
        state.status = LOADING;
        state.error = null;
      })
      .addCase(getChats.fulfilled, (state, action) => {
        state.chats = action.payload;
        state.status = SUCCEEDED;
        state.error = null;
      })
      .addCase(getChats.rejected, (state, action) => {
        state.chats = [];
        state.status = FAILED;
        state.error = action?.error?.message as string;
      });
  },
});

export const chatReducer = chatSlice.reducer;

export const { appendChat } = chatSlice.actions;

export const getChatState = (state: RootState) => state.chats;
