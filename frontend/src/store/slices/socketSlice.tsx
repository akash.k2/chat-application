import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "../../typings/types";

const initialState = {
  socket: {},
};

const socketSlice = createSlice({
  name: "socket",
  initialState,
  reducers: {
    addSocket: (state, action) => {
      state.socket = action.payload;
    },
    removeSocket: (state) => {
      state.socket = {};
    },
  },
});

export const socketReducer = socketSlice.reducer;

export const { addSocket, removeSocket } = socketSlice.actions;

export const getSocketState = (state: RootState) => state.socket;
