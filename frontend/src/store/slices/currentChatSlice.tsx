import { createSlice } from "@reduxjs/toolkit";
import { RootState } from "../../typings/types";

const initialState = {
  currentChatId: "",
  chatUserName: "",
  chatUserId: "",
  chatUserAvatarImage: "",
};

const currentChatSlice = createSlice({
  name: "currentChat",
  initialState,
  reducers: {
    addCurrentChat: (state, action) => {
      state.currentChatId = action.payload.chatId;
      state.chatUserId = action.payload.userId;
      state.chatUserAvatarImage = action.payload.avatarImage;
      state.chatUserName = action.payload.username;
    },
  },
});

export const currentChatReducer = currentChatSlice.reducer;

export const { addCurrentChat } = currentChatSlice.actions;

export const getCurrentChatState = (state: RootState) => state.currentChat;
