import { configureStore } from "@reduxjs/toolkit";
import { userReducer } from "./slices/userSlice.js";
import { chatReducer } from "./slices/chatSlice.js";
import { currentChatReducer } from "./slices/currentChatSlice.js";
import { socketReducer } from "./slices/socketSlice.js";

const store = configureStore({
  reducer: {
    user: userReducer,
    chats: chatReducer,
    currentChat: currentChatReducer,
    socket: socketReducer,
  },
});

export default store;
export type AppDispatch = typeof store.dispatch;
