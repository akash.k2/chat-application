const getTime = (date: Date) => {
  const hours = String(new Date(date).getHours()).padStart(2, "0");
  const minutes = String(new Date(date).getMinutes()).padStart(2, "0");
  return hours + ":" + minutes;
};

export default getTime;
