// Toast
import { toast, ToastOptions } from "react-toastify";

export const IDLE = "idle";

export const LOADING = "loading";

export const SUCCEEDED = "success";

export const FAILED = "fail";

export const UNAUTHORIZED = {
  status: 401,
  message: "Invalid credentials",
};

export const FORBIDDEN = {
  status: 403,
  message: "Unauthorized",
};

export const toastConfig: ToastOptions = {
  position: toast.POSITION.BOTTOM_RIGHT,
  autoClose: 1500,
  theme: "dark",
};
