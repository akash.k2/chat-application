import { NavigateFunction } from "react-router-dom";
import { Socket } from "socket.io-client";

export const logOutUser = (socket: Socket, navigate: NavigateFunction) => {
  socket.disconnect();
  sessionStorage.clear();
  navigate("/login");
  window.location.reload();
};
