import Joi from "joi";

const userIdValidator = Joi.object().keys({
  userId: Joi.string().required().messages({
    "string.empty": `User ID cannot be empty`,
    "any.required": `User ID is required`,
  }),
});

export default userIdValidator;
