import { Router } from "express";

import verifyToken from "../../middlewares/verifyToken.js";

import getAllUsersController from "../controllers/getAllUsersController.js";
import getChatsController from "../controllers/getChatsController.js";

const friendsRouter = Router();

friendsRouter.get("/get-all-users/:userId", verifyToken, getAllUsersController);
friendsRouter.get("/get-friends/:userId", verifyToken, getChatsController);

export default friendsRouter;
