import User from "../../Auth/models/userModel.js";
import CustomError from "../../Utils/ErrorHandlers/CustomError.js";
import { ERROR_UNKNOWN } from "../../Utils/constants.js";

const getUserInfo = async (chats: Array<Chat>) => {
  try {
    const userInfo = await Promise.all(
      [...chats].map(async (chat) => {
        const userId = chat.members[0];
        const detail = await User.find({ _id: userId }).select(
          "username avatarImage"
        );
        return detail[0];
      })
    );
    return userInfo;
  } catch (error) {
    throw new CustomError(ERROR_UNKNOWN.status, ERROR_UNKNOWN.message);
  }
};

export default getUserInfo;
