import globalValidator from "../../Utils/globalValidator.js";
import checkUserById from "../db/checkUserById.js";
import getAllUsers from "../db/getAllUsers.js";
import userIdValidator from "../validators/userIdValidator.js";

const getAllUsersService = async (userId: string) => {
  if (globalValidator(userIdValidator, { userId })) {
    // checks validity of the user id
    await checkUserById(userId);

    //gets all other users details
    const users = await getAllUsers(userId);

    return users;
  }
};

export default getAllUsersService;
