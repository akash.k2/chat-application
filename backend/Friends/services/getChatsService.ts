import globalValidator from "../../Utils/globalValidator.js";
import userIdValidator from "../validators/userIdValidator.js";
import checkUserById from "../db/checkUserById.js";
import getChats from "../db/getChats.js";
import getUserInfo from "./getUserInfo.js";

const getChatsServices = async (userId: string) => {
  if (globalValidator(userIdValidator, { userId })) {
    // checks validity of the user id
    await checkUserById(userId);

    //get all friends details
    const chats = await getChats(userId);

    const usersInfo = await getUserInfo(chats);

    const chatInfo = [...usersInfo].map((user, idx) => {
      return {
        userId: user._id,
        username: user.username,
        avatarImage: user.avatarImage,
        chatId: chats[idx].chatId,
      };
    });

    return chatInfo;
  }
};

export default getChatsServices;
