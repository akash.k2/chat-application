import { ObjectId } from "mongodb";
import User from "../../Auth/models/userModel.js";

const getAllUsers = async (userId: string) => {
  const users = await User.find({ _id: { $nin: new ObjectId(userId) } }).select(
    "_id username avatarImage"
  );

  return users;
};

export default getAllUsers;
