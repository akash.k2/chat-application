import User from "../../Auth/models/userModel.js";

const getChats = async (userId: string) => {
  const chats = await User.find({ _id: userId }).select("chatsInvolved -_id");
  return chats[0].chatsInvolved;
};

export default getChats;
