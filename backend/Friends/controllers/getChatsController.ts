import asyncErrorHandler from "../../Utils/ErrorHandlers/asyncErrorHandler.js";

import getChatsServices from "../services/getChatsService.js";
import { NO_CONTENT, FETCH_SUCCESS } from "../../Utils/constants.js";

const getChatsController = asyncErrorHandler(async (req, res) => {
  const { userId } = req.params;

  const chats = await getChatsServices(userId);

  if (chats?.length === 0) {
    return res.status(NO_CONTENT.status).send({ message: NO_CONTENT.message });
  } else {
    return res.status(FETCH_SUCCESS.status).send({ chats });
  }
});

export default getChatsController;
