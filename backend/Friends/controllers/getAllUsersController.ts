import asyncErrorHandler from "../../Utils/ErrorHandlers/asyncErrorHandler.js";
import getAllUsersService from "../services/getAllUsersService.js";
import { FETCH_SUCCESS, NO_CONTENT } from "../../Utils/constants.js";

const getAllUsersController = asyncErrorHandler(async (req, res) => {
  const { userId } = req.params;

  const users = await getAllUsersService(userId);

  if (users?.length === 0) {
    return res.status(NO_CONTENT.status).send({ message: NO_CONTENT.message });
  } else {
    return res.status(FETCH_SUCCESS.status).send({ users });
  }
});

export default getAllUsersController;
