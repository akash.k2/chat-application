import bcrypt from "bcrypt";

import CustomError from "../../Utils/ErrorHandlers/CustomError.js";
import { UNAUTHORIZED } from "../../Utils/constants.js";

export const hashPassword = async (password: string) => {
  return await bcrypt.hash(password, 10);
};

export const comparePassword = async (
  password: string,
  hashedpassword: string
) => {
  try {
    if (!(await bcrypt.compare(password, hashedpassword))) {
      throw new CustomError(UNAUTHORIZED.status, UNAUTHORIZED.message);
    } else {
      return "Success";
    }
  } catch (e) {
    throw new CustomError(UNAUTHORIZED.status, UNAUTHORIZED.message);
  }
};
