import Joi from "joi";

const registerValidator = Joi.object().keys({
  username: Joi.string().required().messages({
    "string.empty": `Username cannot be empty`,
    "any.required": `Username is required`,
  }),
  email: Joi.string()
    .email({
      minDomainSegments: 2,
      tlds: { allow: ["com", "net", "co", "org", "in"] },
    })
    .required()
    .messages({
      "string.empty": `Email cannot be empty`,
      "any.required": `Email is required`,
    }),
  password: Joi.string().required().messages({
    "string.empty": `Password cannot be empty`,
    "any.required": `Password is required`,
  }),
  avatarImage: Joi.string().required().messages({
    "string.empty": `Kindly set an Avatar image`,
    "any.required": `Kindly set an Avatar image`,
  }),
});

export default registerValidator;
