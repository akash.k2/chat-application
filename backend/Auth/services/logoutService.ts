import globalValidator from "../../Utils/globalValidator.js";
import checkUserById from "../../Chats/db/checkUserById.js";
import userIdValidator from "../../Friends/validators/userIdValidator.js";
import updateLastSeen from "../db/updateLastSeen.js";

const logoutService = async (userId: string) => {
  if (globalValidator(userIdValidator, { userId })) {
    await checkUserById(userId);

    return await updateLastSeen(userId);
  }
};

export default logoutService;