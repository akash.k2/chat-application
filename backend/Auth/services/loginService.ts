import { comparePassword } from "../utils/bcrypt.js";

import globalValidator from "../../Utils/globalValidator.js";
import checkUser from "../db/checkUser.js";
import loginValidator from "../validators/loginValidator.js";
import generateToken from "./generateToken.js";

const loginService = async (username: string, password: string) => {
  if (globalValidator(loginValidator, { username, password })) {
    const user = await checkUser(username);

    if (user.password && user.avatarImage) {
      const isValid = await comparePassword(password, user.password);

      if (isValid === "Success") {
        const token = generateToken(
          username,
          user._id.toString(),
          user?.avatarImage.toString()
        );

        const details = {
          username,
          id: user._id,
          avatarImage: user.avatarImage,
        };

        return {
          token,
          details,
        };
      }
    }
  }
};

export default loginService;
