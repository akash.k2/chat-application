import { hashPassword } from "../utils/bcrypt.js";

import globalValidator from "../../Utils/globalValidator.js";
import registerValidator from "../validators/registerValidator.js";
import createUser from "../db/createUser.js";
import generateToken from "./generateToken.js";

const registerService = async (
  username: string,
  email: string,
  password: string,
  avatarImage: string
) => {
  if (
    globalValidator(registerValidator, {
      username,
      email,
      password,
      avatarImage,
    })
  ) {
    const hashedPassword = await hashPassword(password);

    const newUser = await createUser(
      username,
      email,
      hashedPassword,
      avatarImage
    );

    const token = generateToken(username, newUser._id.toString(), avatarImage);

    const details = {
      username,
      id: newUser._id,
      avatarImage,
    };

    return {
      token,
      details,
    };
  }
};

export default registerService;
