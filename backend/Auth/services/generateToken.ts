import jwt from "jsonwebtoken";

const generateToken = (username: string, id: string, avatarImage: string) => {
  const token = jwt.sign(
    { username, id, avatarImage },
    process.env.TOKEN_SECRET.toString(),
    { expiresIn: "43200s" }
  );

  return token;
};

export default generateToken;
