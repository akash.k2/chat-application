import { Request, Response } from "express";
import { FETCH_SUCCESS, FORBIDDEN } from "../../Utils/constants.js";

const getUserDetails = (req: Request, res: Response) => {
  if (req.user) {
    const details = {
      username: req.user.username,
      id: req.user.id,
      avatarImage: req.user.avatarImage,
    };

    return res.status(FETCH_SUCCESS.status).send({ details });
  } else {
    return res.status(FORBIDDEN.status).send({ message: FORBIDDEN.message });
  }
};

export default getUserDetails;
