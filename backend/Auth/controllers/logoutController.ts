import asyncErrorHandler from "../../Utils/ErrorHandlers/asyncErrorHandler.js";
import { LOGOUT_SUCCESS } from "../../Utils/constants.js";
import logoutService from "../services/logoutService.js";

const logoutController = asyncErrorHandler(async (req, res) => {
  const { userId } = req.body;

  await logoutService(userId);

  return res.status(LOGOUT_SUCCESS.status).send({
    message: LOGOUT_SUCCESS.message,
  });
});

export default logoutController;
