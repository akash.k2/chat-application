import asyncErrorHandler from "../../Utils/ErrorHandlers/asyncErrorHandler.js";
import { REGISTER_SUCCESS } from "../../Utils/constants.js";
import registerService from "../services/registerService.js";

const registerController = asyncErrorHandler(async (req, res) => {
  const { username, email, password, avatarImage } = req.body;

  const result = await registerService(username, email, password, avatarImage);

  return res.status(REGISTER_SUCCESS.status).send({
    message: REGISTER_SUCCESS.message,
    token: result?.token,
    details: result?.details,
  });
});

export default registerController;
