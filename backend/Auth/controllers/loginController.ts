import { Request, Response } from "express";
import asyncErrorHandler from "../../Utils/ErrorHandlers/asyncErrorHandler.js";
import { LOGIN_SUCCESS } from "../../Utils/constants.js";
import loginService from "../services/loginService.js";

const loginController = asyncErrorHandler(
  async (req: Request, res: Response) => {
    const { username, password } = req.body;

    const result = await loginService(username, password);

    return res.status(LOGIN_SUCCESS.status).send({
      message: LOGIN_SUCCESS.message,
      token: result?.token,
      details: result?.details,
    });
  }
);

export default loginController;
