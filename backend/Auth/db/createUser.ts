import User from "../models/userModel.js";

const createUser = async (
  username: string,
  email: string,
  hashedPassword: string,
  avatarImage: string
) => {
  const user = new User({
    username: username.trim().toLowerCase(),
    email: email.trim().toLowerCase(),
    password: hashedPassword,
    avatarImage,
    lastSeen: Date.now()
  });

  const newUser = await user.save();
  return newUser;
};

export default createUser;
