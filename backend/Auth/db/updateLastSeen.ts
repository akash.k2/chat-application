import User from "../models/userModel.js";

const updateLastSeen = async (userId: string) => {
  const lastSeen = Date.now();
  await User.updateOne(
    { _id: userId },
    { lastSeen: lastSeen }
  );
  return lastSeen;
};

export default updateLastSeen;
