import { ObjectId } from "mongodb";
import User from "../models/userModel.js";

const getLastSeenTime = async (userId: string) => {
  const users = await User.find({ _id: { $nin: new ObjectId(userId) } }).select(
    "lastSeen"
  );
  return users;
};

export default getLastSeenTime;
