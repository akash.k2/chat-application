import CustomError from "../../Utils/ErrorHandlers/CustomError.js";
import { UNAUTHORIZED } from "../../Utils/constants.js";
import User from "../models/userModel.js";

const checkUser = async (username: string) => {
  const user = await User.find({ username }).select("_id avatarImage password");
  if (user.length !== 1) {
    throw new CustomError(UNAUTHORIZED.status, UNAUTHORIZED.message);
  } else {
    return user[0];
  }
};

export default checkUser;
