import { Router } from "express";

import verifyToken from "../../middlewares/verifyToken.js";

import loginController from "../controllers/loginController.js";
import registerController from "../controllers/registerController.js";
import getUserDetails from "../controllers/getUserDetails.js";
import logoutController from "../controllers/logoutController.js";

const authRouter = Router();

authRouter.get("/get-details", verifyToken, getUserDetails);

authRouter.post("/register", registerController);

authRouter.post("/login", loginController);

authRouter.post("/logout", verifyToken, logoutController);

export default authRouter;
