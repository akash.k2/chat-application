import { Schema, model } from "mongoose";

const userSchema = new Schema({
  username: {
    type: String,
    trim: true,
    lowercase: true,
    unique: true,
    required: [true, "Enter username"],
  },
  email: {
    type: String,
    unique: true,
    trim: true,
    lowercase: true,
    required: [true, "Enter email address"],
    match: [
      /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/,
      "Enter a valid email address",
    ],
  },
  password: {
    type: String,
    required: [true, "Enter password"],
  },
  avatarImage: {
    type: String,
    required: [true, "Set Avatar"],
  },
  // onlineStatus: {
  //   type: Boolean,
  //   default: true,
  // },
  // loginTime: {
  //   type: Date,
  // },
  lastSeen: {
    type: Date,
  },
  chatsInvolved: {
    type: Array,
    default: [],
    required: true,
    // {Chat id , [receiverIds(userB)]}
  },
});

const User = model("Users", userSchema);

export default User;
