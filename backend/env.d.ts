declare namespace NodeJS {
  interface ProcessEnv {
    MONGO_URL: string;
    TOKEN_SECRET: string;
    PORT: string;
    NODE_ENV: "development" | "production";
  }
}
