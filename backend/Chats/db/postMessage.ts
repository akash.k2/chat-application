import Messages from "../models/messageModel.js";

const postMessage = async (
  senderId: string,
  chatId: string,
  message: string
) => {
  const msg = new Messages({
    chatId,
    senderId,
    timeStamp: Date.now(),
    content: message,
  });
  return await msg.save();
};

export default postMessage;
