import User from "../../Auth/models/userModel.js";

const addChat = async (userA: string, chatId: string, receiverId: string) => {
  const involvedChats = { chatId, members: [receiverId] };
  return await User.updateOne(
    { _id: userA },
    { $push: { chatsInvolved: involvedChats } }
  );
};

export default addChat;
