import Messages from "../models/messageModel.js";

const getMessages = async (chatId: string) => {
  const messages = await Messages.find({ chatId });

  return messages;
};

export default getMessages;
