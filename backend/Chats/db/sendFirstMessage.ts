import Chats from "../models/chatModel.js";
import Messages from "../models/messageModel.js";

import addChat from "./addChat.js";

const sendFirstMessage = async (
  senderId: string,
  receiverId: string,
  message: string
) => {
  const chat = new Chats({
    members: [senderId, receiverId],
  });
  const res = await chat.save();

  const chatId = res._id.toString();
  await addChat(senderId, chatId, receiverId);
  await addChat(receiverId, chatId, senderId);
  const msg = new Messages({
    chatId,
    senderId,
    timeStamp: Date.now(),
    content: message.trim(),
  });
  return await msg.save();
};

export default sendFirstMessage;
