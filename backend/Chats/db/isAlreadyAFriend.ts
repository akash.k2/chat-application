import Chats from "../models/chatModel.js";

const isAlreadyAFriend = async (userA: string, userB: string) => {
  const checkFriend = await Chats.find({
    $or: [{ members: [userA, userB] }, { members: [userB, userA] }],
  });
  if (checkFriend.length === 1) {
    return true;
  }
  return false;
};

export default isAlreadyAFriend;
