import Chats from "../models/chatModel.js";

const checkMember = async (chatId: string, senderId: string) => {
  const check = await Chats.find({
    _id: chatId,
    members: { $in: senderId },
  });
  if (check.length === 1) {
    return true;
  }
  return false;
};

export default checkMember;
