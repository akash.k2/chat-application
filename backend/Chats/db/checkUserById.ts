import User from "../../Auth/models/userModel.js";
import CustomError from "../../Utils/ErrorHandlers/CustomError.js";
import { INVALID_USER_ID } from "../../Utils/constants.js";

const checkUserById = async (user_id: string) => {
  const userId = await User.find({ _id: user_id }).select("_id");
  if (userId.length !== 1) {
    throw new CustomError(INVALID_USER_ID.status, INVALID_USER_ID.message);
  } else {
    return true;
  }
};

export default checkUserById;
