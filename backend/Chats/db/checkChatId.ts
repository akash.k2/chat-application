import Chats from "../models/chatModel.js";
import CustomError from "../../Utils/ErrorHandlers/CustomError.js";
import { INVALID_USER_ID } from "../../Utils/constants.js";

const checkChatId = async (chat_id: string) => {
  const chatId = await Chats.find({ _id: chat_id }).select("_id");
  if (chatId.length !== 1) {
    throw new CustomError(INVALID_USER_ID.status, "Invalid chat Id");
  } else {
    return true;
  }
};

export default checkChatId;
