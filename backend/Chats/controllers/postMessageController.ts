import asyncErrorHandler from "../../Utils/ErrorHandlers/asyncErrorHandler.js";
import { INSERT_SUCCESS } from "../../Utils/constants.js";
import postMessageService from "../services/postMessageService.js";

const postMessageController = asyncErrorHandler(async (req, res) => {
  const { senderId,chatId, message } = req.body;
  await postMessageService(senderId, chatId, message);
  res.status(INSERT_SUCCESS.status).send({message:INSERT_SUCCESS.message});
});

// add to friend list
export default postMessageController;