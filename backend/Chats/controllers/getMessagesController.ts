import asyncErrorHandler from "../../Utils/ErrorHandlers/asyncErrorHandler.js";
import { FETCH_SUCCESS, NO_CONTENT } from "../../Utils/constants.js";

import getMessagesService from "../services/getMessagesService.js";

const getMessagesController = asyncErrorHandler(async (req, res) => {
  const { chatId } = req.params;

  const messages = await getMessagesService(chatId);

  if (messages?.length !== 0) {
    res.status(FETCH_SUCCESS.status).send({ messages });
  } else {
    res.status(NO_CONTENT.status).send({ message: NO_CONTENT.message });
  }
});

export default getMessagesController;
