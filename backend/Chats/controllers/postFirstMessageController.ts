import asyncErrorHandler from "../../Utils/ErrorHandlers/asyncErrorHandler.js";
import { INSERT_SUCCESS } from "../../Utils/constants.js";
import postFirstMessageService from "../services/postFirstMessageService.js";

const postFirstMessageController = asyncErrorHandler(async (req, res) => {
  const { senderId, receiverId, message } = req.body;
  const result = await postFirstMessageService(senderId, receiverId, message);
  res
    .status(INSERT_SUCCESS.status)
    .send({ message: INSERT_SUCCESS.message, chatId: result?.chatId });
});

export default postFirstMessageController;
