import globalValidator from "../../Utils/globalValidator.js";
import checkChatId from "../db/checkChatId.js";
import getMessages from "../db/getMessages.js";
import getMessagesValidator from "../validators/getMessagesValidator.js";

const getMessagesService = async (chatId: string) => {
  if (globalValidator(getMessagesValidator, { chatId })) {
    await checkChatId(chatId);

    const messages = await getMessages(chatId);

    return messages;
  }
};

export default getMessagesService;
