import globalValidator from "../../Utils/globalValidator.js";
import sendFirstMessageValidator from "../validators/postMessageValidator.js";
import postMessage from "../db/postMessage.js";
import checkChatId from "../db/checkChatId.js";

import checkMember from "../db/checkMember.js";
import CustomError from "../../Utils/ErrorHandlers/CustomError.js";
import { INVALID_MEMBER } from "../../Utils/constants.js";
import checkUserById from "../db/checkUserById.js";

const postMessageService = async (
  senderId: string,
  chatId: string,
  message: string
) => {
  if (
    globalValidator(sendFirstMessageValidator, {
      senderId,
      chatId,
      message,
    })
  ) {
    // check whether sender & receiver id are same
    await checkUserById(senderId);
    await checkChatId(chatId);

    if (!(await checkMember(chatId, senderId))) {
      throw new CustomError(INVALID_MEMBER.status, INVALID_MEMBER.message);
    }

    await postMessage(senderId, chatId, message);
  }
};

export default postMessageService;
