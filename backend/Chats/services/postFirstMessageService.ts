import CustomError from "../../Utils/ErrorHandlers/CustomError.js";
import { INVALID_USER_ID } from "../../Utils/constants.js";
import checkUserById from "../db/checkUserById.js";

import globalValidator from "../../Utils/globalValidator.js";

import sendFirstMessage from "../db/sendFirstMessage.js";
import isAlreadyAFriend from "../db/isAlreadyAFriend.js";
import postFirstMessageValidator from "../validators/postFirstMessageValidator.js";

const postFirstMessageService = async (
  senderId: string,
  receiverId: string,
  message: string
) => {
  if (senderId === receiverId) {
    throw new CustomError(INVALID_USER_ID.status, INVALID_USER_ID.message);
  }

  if (
    globalValidator(postFirstMessageValidator, {
      senderId,
      receiverId,
      message,
    })
  ) {
    await checkUserById(senderId);
    await checkUserById(receiverId);

    if (await isAlreadyAFriend(senderId, receiverId)) {
      return;
    }

    return await sendFirstMessage(senderId, receiverId, message);
  }
};

export default postFirstMessageService;
