import mongoose, { Schema, model } from "mongoose";


const messageSchema = new Schema({
    // message id default _id
    chatId:{
        type:mongoose.Types.ObjectId,
        required:[true,"Enter chat id"],
        ref:'Chats'
        // Foreign key
    },
    senderId:{
        type:mongoose.Types.ObjectId,
        required:true,
        ref:'User'
    },
    timeStamp:{
        type:Date,
        default:Date.now
    },
    content:{
        type:String,
        required:true
    },
    readReceipt:{
        type:Array,
        default:[],
        // Format - [{readBy : userId, readTime : Time, deliveredTime: Time}]
    }
})

const Messages = model("Messages", messageSchema);

export default Messages;