import { Schema, model } from "mongoose";

const chatSchema = new Schema({
  // chat id is default mongoose _id
  members: {
    type: Array,
    min: 2,
    required: [true, "Enter members"],
  },
  groupName: {
    type: String,
  },
  groupAvatar: {
    type: String,
  },
  // messages - {sender, message, timeStamp, read}
});

const Chats = model("Chats", chatSchema);

export default Chats;
