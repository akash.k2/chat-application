import { Router } from "express";

import verifyToken from "../../middlewares/verifyToken.js";

import postFirstMessageController from "../controllers/postFirstMessageController.js";
import postMessageController from "../controllers/postMessageController.js";
import getMessagesController from "../controllers/getMessagesController.js";

const chatRouter = Router();

chatRouter.get("/get-messages/:chatId", verifyToken, getMessagesController);

chatRouter.post("/first-message", verifyToken, postFirstMessageController);

chatRouter.post("/post-message", verifyToken, postMessageController);

export default chatRouter;
