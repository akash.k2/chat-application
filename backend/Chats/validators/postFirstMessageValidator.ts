import Joi from "joi";

const postFirstMessageValidator = Joi.object().keys({
  senderId: Joi.string().required().messages({
    "string.empty": `Sender id cannot be empty`,
    "any.required": `Sender id is required`,
  }),
  receiverId: Joi.string().required().messages({
    "string.empty": `Receiver id cannot be empty`,
    "any.required": `Receiver id is required`,
  }),
  message: Joi.string().required().messages({
    "string.empty": `Message cannot be empty`,
    "any.required": `Message is required`,
  }),
});

export default postFirstMessageValidator;
