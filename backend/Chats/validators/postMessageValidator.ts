import Joi from "joi";

const sendFirstMessageValidator = Joi.object().keys({
  senderId: Joi.string().required().messages({
    "string.empty": `Sender id cannot be empty`,
    "any.required": `Sender id is required`,
  }),
  chatId: Joi.string().required().messages({
    "string.empty": `Chat id cannot be empty`,
    "any.required": `Chat id is required`,
  }),
  message: Joi.string().required().messages({
    "string.empty": `Message cannot be empty`,
    "any.required": `Message is required`,
  }),
});

export default sendFirstMessageValidator;
