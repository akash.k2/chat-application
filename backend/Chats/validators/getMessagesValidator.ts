import Joi from "joi";

const getMessagesValidator = Joi.object().keys({
  chatId: Joi.string().required().messages({
    "string.empty": `Chat ID cannot be empty`,
    "any.required": `Chat ID is required`,
  }),
});

export default getMessagesValidator;
