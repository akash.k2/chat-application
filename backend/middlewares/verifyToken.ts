import jwt, { JwtPayload } from "jsonwebtoken";
import { FORBIDDEN } from "../Utils/constants.js";
import { NextFunction, Request, Response } from "express";

const verifyToken = (req: Request, res: Response, next: NextFunction) => {
  const authHeader = req.headers.authorization;

  const bearer = authHeader && authHeader.split(" ")[0];
  const token = authHeader && authHeader.split(" ")[1];

  if (bearer !== "Bearer" || token === null) {
    return res.status(FORBIDDEN.status).send({ message: FORBIDDEN.message });
  }

  try {
    if (token) {
      const decoded = jwt.verify(
        token,
        process.env.TOKEN_SECRET.toString()
      ) as JwtPayload;

      const user: User = {
        id: decoded.id,
        username: decoded.username,
        avatarImage: decoded.avatarImage,
      };
      req.user = user;
      next();
    }
  } catch (error) {
    res.status(FORBIDDEN.status).send({ message: FORBIDDEN.message });
  }
};

export default verifyToken;
