import { Namespace } from "socket.io";
import logoutService from "../../Auth/services/logoutService.js";
import getLastSeenTime from "../../Auth/db/getLastSeenTime.js";

const userSocket = (userNameSpace: Namespace) => {
  // Connection
  let onlineUsers = new Map();
  let lastSeenUsers = new Map<String, any>();

  userNameSpace.on("connection", async (socket) => {
    const socketId = socket.id;
    const userId = socket.handshake.auth.userId;

    // add userid and socket id to MAP structure
    onlineUsers.set(userId, socketId);

    const lastSeenTime = await getLastSeenTime(userId);
    for (let i = 0; i < lastSeenTime.length; i++) {
      lastSeenUsers.set(
        lastSeenTime[i]._id.toString(),
        lastSeenTime[i]?.lastSeen
      );
    }

    userNameSpace.emit("LAST_SEEN", Array.from(lastSeenUsers));

    userNameSpace.emit("GET_ONLINE_USERS", Array.from(onlineUsers.keys()));

    socket.on("SEND_MESSAGE", (data) => {
      const receiverSocket = onlineUsers.get(data.receiverId);
      userNameSpace.to(receiverSocket).emit("RECEIVE_MESSAGE", {
        chatId: data.chatId,
        senderId: data.senderId,
        timeStamp: data.timeStamp,
        content: data.content,
        readReceipt: data.readReceipt,
      });
    });

    socket.on("SEND_FIRST_MESSAGE", (data) => {
      const receiverSocket = onlineUsers.get(data.receiverId);
      userNameSpace.to(receiverSocket).emit("RECEIVE_CHAT", data);
    });

    socket.on("disconnect", async () => {
      console.log("Disconnected");
      
      onlineUsers.delete(userId);
      
      // update user with time
      const lastSeenTime = await logoutService(userId);
      lastSeenUsers.set(userId, lastSeenTime);

      // Emit the updated list of online users to all connected sockets
      userNameSpace.emit("GET_ONLINE_USERS", Array.from(onlineUsers.keys()));
      userNameSpace.emit("LAST_SEEN", Array.from(lastSeenUsers));
    });
  });
};

export default userSocket;
