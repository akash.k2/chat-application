export const REGISTER_SUCCESS = {
  status: 201,
  message: "Registered successfully",
};

export const LOGIN_SUCCESS = {
  status: 200,
  message: "Logged in successfully",
};

export const LOGOUT_SUCCESS = {
  status: 200,
  message: "Logged out successfully",
};

export const FETCH_SUCCESS = {
  status: 200,
};

export const INSERT_SUCCESS = {
  status: 201,
  message: "Send message successfully",
};

export const NO_CONTENT = {
  status: 204,
  message: "No content available",
};

export const EMAIL_ALREADY_EXIST = {
  status: 400,
  message: "Email already exist",
};

export const USERNAME_ALREADY_TAKEN = {
  status: 400,
  message: "Username is already taken",
};

export const INVALID_USER_ID = {
  status: 400,
  message: "Invalid User ID",
};

export const INVALID_ID_VALUE = {
  status: 400,
  message: "Invalid ID value",
};

export const INVALID_MEMBER = {
  status: 400,
  message: "The user is not a member of this chat",
};

export const UNAUTHORIZED = {
  status: 401,
  message: "Invalid credentials",
};

export const FORBIDDEN = {
  status: 403,
  message: "Unauthorized",
};

export const NOT_FOUND = {
  status: 404,
  message: "URL not found",
};

export const ERROR_UNKNOWN = {
  status: 500,
  message: "Something went wrong!",
};
