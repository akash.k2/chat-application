import CustomError from "./ErrorHandlers/CustomError.js";

const globalValidator = (func: any, param: Object) => {
  const validatorObj = func.validate(param);

  if (validatorObj.error) {
    throw new CustomError(400, validatorObj.error.details[0].message);
  } else {
    return true;
  }
};

export default globalValidator;
