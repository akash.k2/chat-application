import { NextFunction, Request, Response } from "express";

type AsyncErrorHandler = (
  req: Request,
  res: Response,
  next: NextFunction
) => Promise<any>;

export default (func: AsyncErrorHandler) => {
  return (req: Request, res: Response, next: NextFunction) => {
    func(req, res, next).catch(async (err: Object) => {
      return next(err);
    });
  };
};
