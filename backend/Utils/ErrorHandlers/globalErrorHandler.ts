import CustomError from "./CustomError.js";

import {
  EMAIL_ALREADY_EXIST,
  USERNAME_ALREADY_TAKEN,
  INVALID_ID_VALUE,
  ERROR_UNKNOWN,
} from "../constants.js";
import { NextFunction, Request, Response } from "express";

const castErrorHandler = (err: CustomError) => {
  return new CustomError(
    INVALID_ID_VALUE.status,
    `${INVALID_ID_VALUE.message} for ${err?.path}`
  );
};

const duplicateKeyErrorHandler = (err: CustomError) => {
  if (err?.keyValue?.email) {
    return new CustomError(
      EMAIL_ALREADY_EXIST.status,
      EMAIL_ALREADY_EXIST.message
    );
  }
  return new CustomError(
    USERNAME_ALREADY_TAKEN.status,
    USERNAME_ALREADY_TAKEN.message
  );
};

const validationErrorHandler = (error: CustomError) => {
  let errorMessage = "";
  if (error?.errors) {
    const err = Object.values(error?.errors).map((val) => val.message);
    errorMessage = err.join(". ");
  }
  return new CustomError(400, errorMessage);
};

const devErrors = (res: Response, error: CustomError) => {
  return res.status(error.statusCode).send({
    status: error.statusCode,
    message: error.message,
    stackTrace: error.stack,
  });
};

const prodErrors = (res: Response, error: CustomError) => {
  if (error.isOperational) {
    return res.status(error.statusCode).send({
      status: error.statusCode,
      message: error.message,
    });
  } else {
    return res.status(ERROR_UNKNOWN.status).send({
      type: "server error",
      message: ERROR_UNKNOWN.message,
    });
  }
};

export default (
  error: CustomError,
  req: Request,
  res: Response,
  next: NextFunction
) => {
  error.statusCode = error.statusCode || 500;
  error.type = error.type || "server error";

  if (error.name === "CastError") {
    error = castErrorHandler(error);
  }
  if (error.code === 11000) {
    error = duplicateKeyErrorHandler(error);
  }
  if (error.name === "ValidationError") {
    error = validationErrorHandler(error);
  }

  if (process.env.NODE_ENV === "development") {
    devErrors(res, error);
  } else if (process.env.NODE_ENV === "production") {
    prodErrors(res, error);
  }
};
