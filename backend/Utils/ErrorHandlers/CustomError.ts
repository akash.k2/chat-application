export default class CustomError extends Error {
  statusCode: number;
  type: string;
  isOperational: boolean;
  code?: number;
  path?: string;
  keyValue?: {
    [key: string]: { email: string; username: string };
  };
  errors?: {
    [key: string]: { message?: string };
  };

  constructor(statusCode: number, message: string) {
    super(message);
    this.statusCode = statusCode;
    this.type =
      statusCode >= 400 && statusCode < 500 ? "bad request" : "server error";
    this.isOperational = true;

    Error.captureStackTrace(this, this.constructor);
  }
}
