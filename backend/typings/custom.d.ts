import { Request } from "express";

declare global {
  interface User {
    username: string;
    id: string;
    avatarImage: string;
  }
  interface Chat {
    members: Array<string>;
  }
}

declare global {
  namespace Express {
    interface Request {
      user: User;
    }
  }
}
