import dotenv from "dotenv";
dotenv.config();

import express, { json } from "express";
import bodyParser from "body-parser";
import { default as mongoose } from "mongoose";
import cors from "cors";

import CustomError from "./Utils/ErrorHandlers/CustomError.js";
import globalErrorHandler from "./Utils/ErrorHandlers/globalErrorHandler.js";

import { NOT_FOUND } from "./Utils/constants.js";

import authRouter from "./Auth/routes/routes.js";
import friendsRouter from "./Friends/routes/routes.js";
import chatRouter from "./Chats/routes/chatRoutes.js";

import { Server } from "socket.io";
import { createServer } from "http";

// user socket
import userSocket from "./socket/namespaces/userSocket.js";

const app = express();

app.use(
  cors({
    origin: "*",
    credentials: true,
    optionsSuccessStatus: 200,
  })
);

app.use(json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use(authRouter);
app.use(chatRouter);
app.use(friendsRouter);

// Socket setup
const server = createServer(app);

// Web socket initialization
const io = new Server(server, {
  cors: {
    origin: "*",
  },
});

const userNameSpace = io.of("/user-namespace");
userSocket(userNameSpace);

app.all("*", (req, res, next) => {
  const error = new CustomError(NOT_FOUND.status, NOT_FOUND.message);
  next(error);
});

app.use(globalErrorHandler);

try {
  await mongoose.connect(process.env.MONGO_URL);
  console.log("DB connected");
  server.listen(process.env.PORT, () => {
    console.log("Server started");
  });
} catch {
  (err: String) => {
    console.log(err);
  };
}

export default app;
